# Configuration

```bash
chmod u+x setup.sh
sudo ./setup.sh
```

# Run Api on 'games' folder

```bash
rackup
```

# Run Client on 'games-client' folder

```bash
chmod u+x games-client
./games-client
```





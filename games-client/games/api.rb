require "rest-client"
require "json"
require_relative "config.rb"

module Api
  class HangmanApi
    def self.get_word(pid:)
      response = RestClient::Request.new(
        :method => :get,
        :url => "#{Config::BASE_URL}/hangman/#{pid}",
      ).execute
      JSON.parse(response.to_str)
    end

    def self.try_letter(pid:, letter:)
      response = RestClient::Request.new(
        :method => :get,
        :url => "#{Config::BASE_URL}/hangman/#{pid}/try/#{letter}",
      ).execute
      JSON.parse(response.to_str)
    end

    def self.get_valid_pid
      pid = 0
      response = Api::HangmanApi.get_word(pid: pid)
      until response[":chances"] == 8
        pid = Random.rand(1000)
        response = Api::HangmanApi.get_word(pid: pid)
      end
      return pid
    end
  end

  class PuzzleApi
    def self.get_matrix(pid:, size:)
      response = RestClient::Request.new(
        :method => :get,
        :url => "#{Config::BASE_URL}/puzzle/#{pid}/#{size}",
      ).execute
      JSON.parse(response.to_str)
    end

    def self.move(pid:, direction:)
      response = RestClient::Request.new(
        :method => :get,
        :url => "#{Config::BASE_URL}/puzzle/#{pid}/move/#{direction}",
      ).execute
      JSON.parse(response.to_str)
    end

    def self.get_valid_pid(size:)
      pid = 0
      response = Api::PuzzleApi.get_matrix(pid: pid, size: size)
      until response[":actual_moves"] == 0
        pid = Random.rand(1000)
        response = Api::PuzzleApi.get_matrix(pid: pid, size: size)
      end
      return pid
    end
  end
end

require_relative("api.rb")

class Puzzle
  attr_reader :state

  def initialize(size:)
    @pid = Api::PuzzleApi.get_valid_pid(size: size)
    @size = size
    response = Api::PuzzleApi.get_matrix(pid: @pid, size: size)
    @state = "playing"
    map response
  end

  def win?
    @state == "win"
  end

  def show_state
    print_board
    puts "actual moves: #{@actual_moves}"
    puts "maximum moves: #{@max_moves}"
  end

  def move(direction:)
    response = Api::PuzzleApi.move(pid: @pid, direction: direction.downcase)
    @state = response[":state"]
    map response
  end

  private

  def print_board
    (0..@size - 1).each do |i|
      (0..@size - 1).each do |j|
        print("#{@matrix[i * @size + j] ? @matrix[i * @size + j] : "_"}\t")
      end
      print("\n")
    end
  end

  def map(response)
    puts response
    @matrix = response[":matrix"]
    @actual_moves = response[":actual_moves"]
    @max_moves = response[":max_moves"]
  end
end

require_relative("api.rb")

class Hangman
  attr_reader :state

  def initialize
    @pid = Api::HangmanApi.get_valid_pid
    response = Api::HangmanApi.get_word(pid: @pid)
    @state = "playing"
    map response
  end

  def try_letter(letter:)
    response = Api::HangmanApi.try_letter(pid: @pid, letter: letter)
    @last_letter = letter
    @last_try = response[":success"]
    @state = response[":state"]
  end

  def update
    response = Api::HangmanApi.get_word(pid: @pid)
    map response
  end

  def show_state
    puts("word: #{@word}")
    puts("hint: #{@hint}")
    puts("chances: #{@chances}")
    puts("last letter: #{@last_letter}")
    puts("last try: #{@last_letter}")
  end

  def win?
    @state == "win"
  end

  private

  def map(response)
    @word = response[":word"]
    @hint = response[":hint"]
    @attempts = response[":attempts"]
    @chances = response[":chances"]
    @failures = response[":failures"]
  end
end

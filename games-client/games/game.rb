require_relative("hangman.rb")
require_relative("puzzle.rb")

class Game
  def show_initial_screen
    puts(["H to play Hangman", "P to play a puzzle", "Q to quit"])
    letter = gets.chomp
    if letter.upcase[0] == "H"
      hangman = Hangman.new
      show_hangman_screen(hangman)
      show_final_screen(hangman)
    elsif letter.upcase[0] == "P"
      size = get_puzzle_size
      puzzle = Puzzle.new(size: size)
      show_puzzle_screen(puzzle)
      show_final_screen(puzzle)
    elsif letter.upcase[0] == "Q"
      puts("Thanks to play")
    else
      show_initial_screen
    end
  end

  def get_puzzle_size
    puts "Give me a number for the board size"
    number = gets.chomp.to_i
    if number.nil?
      return get_puzzle_size
    else
      return number
    end
  end

  def show_hangman_screen(hangman)
    while hangman.state == "playing"
      hangman.show_state
      puts("Enter a key to try, or 9 to quit")
      letter = gets.chomp
      letter = letter.upcase[0].nil? ? "0" : letter
      if letter == "9"
        break
      end
      if letter.upcase[0] > "Z" or letter.upcase[0] < "A"
        puts("Please enter a valid character")
      else
        hangman.try_letter(letter: letter)
        if hangman.state == "playing"
          hangman.update
        end
      end
    end
  end

  def show_puzzle_screen(puzzle)
    while puzzle.state == "playing"
      directions = ["UP", "DOWN", "LEFT", "RIGHT"]
      puzzle.show_state
      puts("Enter one of the following directions, to move the _ number, or 0 to quit")
      puts(directions)
      direction = gets.chomp
      direction = direction.upcase[0].nil? ? "0" : direction
      if direction == "0"
        break
      end
      if not directions.include? direction.upcase
        puts("Please enter a valid word")
      else
        puzzle.move(direction: direction)
      end
    end
  end

  def show_final_screen(a_duck)
    if a_duck.win?
      puts("Done, you are a winner")
    else
      puts("Sorry, you lose")
    end
    show_initial_screen
  end
end

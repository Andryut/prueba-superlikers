module Games
  class PuzzleApi < Sinatra::Base
    get "/puzzle/:participation_id/:size" do
      @puzzle = Puzzle.where(participation_id: params[:participation_id]).first
      player = Player.where(fk: params[:participation_id]).first
      player = Player.create!(fk: params[:participation_id]) unless player

      if @puzzle.nil?
        player.record("puzzle")

        matrix = Puzzle.create_matrix(params[:size])
        @puzzle = Puzzle.new(
          participation_id: params[:participation_id],
        )
        @puzzle.matrix = matrix
        @puzzle.save!
      elsif @puzzle.state != "playing"
        player.record("puzzle")
        matrix = Puzzle.create_matrix(params[:size])
        @puzzle.matrix = matrix
        @puzzle.start(params[:size])
        @puzzle.save!
      end

      return Oj.dump(
               matrix: @puzzle.matrix,
               max_moves: @puzzle.max_moves,
               actual_moves: @puzzle.actual_moves,
             )
    end

    get "/puzzle/:participation_id/move/:direction" do
      @puzzle = Puzzle.where(participation_id: params[:participation_id]).first
      ok = @puzzle.move(params[:direction])
      return Oj.dump(
               matrix: @puzzle.matrix,
               success: ok,
               state: @puzzle.state,
               max_moves: @puzzle.max_moves,
               actual_moves: @puzzle.actual_moves,
             )
    end
  end
end

module Games
  class Puzzle
    include Mongoid::Document

    field :i_void, type: Integer
    field :j_void, type: Integer
    field :matrix, type: Array, default: []
    field :participation_id, type: String
    field :size, type: Integer, default: 0
    field :max_moves, type: Integer
    field :actual_moves, type: Integer

    index({ participation_id: 1 }, unique: true)

    def state
      if win?
        return "win"
      elsif actual_moves > max_moves
        return "lose"
      end
      "playing"
    end

    def win?
      self[:matrix] == self[:matrix].sort_by { |number| [number ? 0 : 1, number] }
    end

    def start(size)
      self.matrix = Puzzle.create_matrix(size)
      self.actual_moves = 0
      self.max_moves = 0
      self.i_void = size.to_i - 1
      self.j_void = size.to_i - 1
      self.size = size.to_i
      unsort_matrix
    end

    def move(direction)
      moves = { "left" => [0, -1], "right" => [0, 1], "up" => [-1, 0], "down" => [1, 0] }
      unless moves.keys.include? direction and check(moves[direction])
        return false
      end
      matrix[i_void * size + j_void] = matrix[(i_void + moves[direction][0]) * size + (j_void + moves[direction][1])]
      matrix[(i_void + moves[direction][0]) * size + (j_void + moves[direction][1])] = nil
      self.i_void += moves[direction][0]
      self.j_void += moves[direction][1]
      self.actual_moves += 1
      save
      return true
    end

    def unsort_matrix
      moves = [[0, -1], [1, 0], [-1, 0], [1, 0]]
      (500..Random.rand(5000)).each do |number|
        direction = moves[Random.rand(4)]
        if check(direction)
          matrix[i_void * size + j_void] = matrix[(i_void + direction[0]) * size + (j_void + direction[1])]
          matrix[(i_void + direction[0]) * size + (j_void + direction[1])] = nil
          self.i_void += direction[0]
          self.j_void += direction[1]
          self.max_moves += 1
        end
      end
    end

    def self.create_matrix(size)
      output = []
      (1..(size.to_i * size.to_i)).each do |number|
        output.push(number.to_i)
      end
      output[(size.to_i * size.to_i) - 1] = nil
      return output
    end

    private

    def check(move)
      return move[0] + self.i_void < self.size && move[0] + self.i_void >= 0 && move[1] + self.j_void < self.size && move[1] + self.j_void >= 0
    end
  end
end
